// To parse this JSON data, do
//
//     final uploadedImageListPojo = uploadedImageListPojoFromJson(jsonString);

import 'dart:convert';

UploadedImageListPojo uploadedImageListPojoFromJson(String str) => UploadedImageListPojo.fromJson(json.decode(str));

String uploadedImageListPojoToJson(UploadedImageListPojo data) => json.encode(data.toJson());

class UploadedImageListPojo {
  List<String> files;
  String status;
  String message;

  UploadedImageListPojo({
    this.files,
    this.status,
    this.message,
  });

  factory UploadedImageListPojo.fromJson(Map<String, dynamic> json) => UploadedImageListPojo(
    files: List<String>.from(json["files"].map((x) => x)),
    status: json["status"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "files": List<dynamic>.from(files.map((x) => x)),
    "status": status,
    "message": message,
  };
}
