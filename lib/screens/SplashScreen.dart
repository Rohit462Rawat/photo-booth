import 'dart:async';

import 'package:flutter/material.dart';
import 'package:photobooth/screens/AfterSplashScreen.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'HomeScreen.dart';




class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences prefs;
  startTime() async {
    prefs = await SharedPreferences.getInstance();
    var _duration = new Duration(seconds: 2);

    if(prefs.getBool('is_login')==true){
      return new Timer(_duration, navigationHomePage);
    }else{
      return new Timer(_duration, navigationLoginPage);
    }

  }


  void navigationLoginPage() {
       Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => AfterSplashScreen()),
            (Route<dynamic> route) => false);
  }

  void navigationHomePage() {
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen()),
            (Route<dynamic> route) => false);
  }


  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      //backgroundColor: color_white,
      body: Container(
       /* margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
        padding: EdgeInsets.all(70),*/

        //padding: EdgeInsets.fromLTRB(60,150,60,150),
        child: Stack(
          children: <Widget>[
            Image.asset('assets/images/splash.png',
              fit: BoxFit.fill,
              height: double.infinity,
              width: double.infinity,
              /* alignment: Alignment.center,*/),

            Center(child:

            Container(
              margin: EdgeInsets.only(left: 60,right: 55),
              child: Image.asset('assets/images/splashlogo.png',
                fit: BoxFit.cover,
                //height: double.infinity,
               // width: double.infinity,
                /* alignment: Alignment.center,*/),
            ),)
          ],
        ),
      ),
    );
  }
}