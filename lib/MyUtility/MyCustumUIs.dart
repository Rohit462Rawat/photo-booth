import 'package:flutter/material.dart';
import 'package:photobooth/MyUtility/theme/myStyle.dart';


dropDown() {
  return DropdownButton<String>(
    items: <String>['+91', '+1', '+87', '+76'].map((String value) {
      return new DropdownMenuItem<String>(
        value: value,
        child: new Text(value),
      );
    }).toList(),
    onChanged: (_) {},
  );
}

custumTextview(text, fontsize) {
  return Text(text,
      style: TextStyle(color: Colors.grey, fontSize: fontsize),
      textAlign: TextAlign.center);
}

custumTextviewRegular(text, fontsize) {
  return Text(
    text,
    style: TextStyle(
        color: Colors.black,
        fontSize: fontsize,
        fontFamily: 'Roboto-Regular',
        fontWeight: FontWeight.normal),
    textAlign: TextAlign.center,
  );
}


custumTextviewBold(text, fontsize) {
  return Text(
    text,
    style: TextStyle(
        color: Colors.black,
        fontSize: fontsize,
        fontFamily: 'Roboto-Bold',
        fontWeight: FontWeight.bold),
    textAlign: TextAlign.center,
  );
}

Widget custumButtonWithShape(text, color) {
  return Container(
      height: 50,
      width: double.infinity,
      child: FlatButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(3.0),
        ),
        color: color,
        textColor: Colors.white,
        disabledColor: color,
        disabledTextColor: Colors.white,
        //because without onpressed fat button not show color and text color properly

        child: Text(
          text,
          style: TextStyle(
              fontSize: 16.0,
              fontFamily: 'Roboto-Regular',
             // fontWeight: FontWeight.bold
          ),
        ),
      ));
}





Widget custumButtonWithShapeSize(text, color,size) {
  return Container(
      height: size,
      width: double.infinity,
      child: FlatButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(3.0),
        ),
        color: color,
        textColor: Colors.white,
        disabledColor: color,
        disabledTextColor: Colors.white,
        //because without onpressed fat button not show color and text color properly

        child: Text(
          text,
          style: TextStyle(
            fontSize: 16.0,
            fontFamily: 'Roboto-Regular',
            // fontWeight: FontWeight.bold
          ),
        ),
      ));
}



Widget custumButtonWithShapeSizeTranparentBack(text, bordercolor,size) {
  return Container(
      height: size,
      width: 150,
      decoration: BoxDecoration(
          border: Border.all(
            color: bordercolor,
            width: 1.5,

          ),
        borderRadius: new BorderRadius.circular(50.0),
      ),
      child: Center(
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18.0,
            color: primaryColor,
            fontFamily: 'Roboto-Regular',
             fontWeight: FontWeight.bold
          ),
        ),
      ));
}


Widget textFieldBackgroundUI(widget, textField) {
  return Container(
    height: 50,
    decoration: new BoxDecoration(
      color: textfieldbackColor,
     // border: new Border.all(color: Colors.grey),
      borderRadius: const BorderRadius.all(
        const Radius.circular(3.0),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[

        Container(
          child: Flexible(
            child: textField,
          ), //flexible
        ), //container

        Container(
            margin:
            const EdgeInsets.only(left: 10, top: 10, right: 15, bottom: 10),
            child: widget),
      ], //widget
    ),
  );
}

Widget textFieldBackgroundUI2(text, textField) {
  return Container(
    height: 55,
    margin: const EdgeInsets.only(left: 0, top: 15, right: 0),
    // padding: const EdgeInsets.all(3.0),

    child: Column(
      children: <Widget>[
        Container(
            margin: const EdgeInsets.only(left: 0, right: 0, bottom: 15),
            child: Align(
              alignment: Alignment.topLeft,
              child: text,
            )
            //flexible
            ),
        Container(
          child: Flexible(
            child: textField,
          ), //flexible
        ), //container
      ], //widget
    ),
  );
}

Widget textFieldBackgroundUI1(icon, textField) {
  return Container(
    height: 45,
    margin: const EdgeInsets.only(left: 0, top: 15, right: 0),
    // padding: const EdgeInsets.all(3.0),
    decoration: new BoxDecoration(
      border: new Border.all(color: Colors.pink),
      borderRadius: const BorderRadius.all(
        const Radius.circular(30.0),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            margin:
                const EdgeInsets.only(left: 12, top: 10, right: 10, bottom: 10),
            child: icon),
        Container(
          child: Flexible(
            child: textField,
          ), //flexible
        ), //container
      ], //widget
    ),
  );
}

Widget toolbarLayoutTransparentBackground(context) {
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0.1,
    leading: new IconButton(
      icon: SizedBox.fromSize(
        child: Image.asset(
          'assets/images/arrow-back-black.svg',
        ),
        // size: Size(300.0, 400.0),
      ),
      onPressed: () => Navigator.of(context).pop(),
    ),
    // title: Text(name,style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold),),
    //centerTitle: true,
  );
}

Widget toolbarLayout(name, context) {
  return AppBar(
    backgroundColor: Colors.black,
    elevation: 0.0,
    /*leading: new IconButton(
      icon: SizedBox.fromSize(
        child:Icon(Icons.arrow_back,color: Colors.black,),
        // size: Size(300.0, 400.0),
      ),
      onPressed: () => Navigator.of(context).pop(),
    ),*/
    title: Text(
      name,
      style: TextStyle(
          color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
    ),
    centerTitle: true,
  );
}

Widget custuTtoolbarLayoutBlackArrow(context) {
  return GestureDetector(
    onTap: () => Navigator.of(context).pop(),
    child: Container(
        margin: EdgeInsets.only(left: 20, top: 20, right: 20),

        child: SizedBox(
            width: 20,
            height: 20,
            child: Image.asset("assets/images/back_black.png",))),
  );
}



Widget custuTtoolbarLayoutWhiteArrow(context) {
  return GestureDetector(
    onTap: () => Navigator.of(context).pop(),
    child: Container(
        margin: EdgeInsets.only(left: 20, top: 20, right: 20),

        child: SizedBox(
            width: 20,
            height: 20,
            child: Image.asset("assets/images/back_white.png",))),
  );
}


Widget toolbarLayoutWithCrossBackButton(name, context) {
  return AppBar(
    backgroundColor: Colors.white,
    elevation: 0.9,
    leading: new IconButton(
      icon: SizedBox.fromSize(
        child: Image.asset(
          'assets/images/close-black.svg',
        ),
        // size: Size(300.0, 400.0),
      ),
      onPressed: () => Navigator.of(context).pop(),
    ),
    title: Text(
      name,
      style: TextStyle(
          color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
    ),
    centerTitle: true,
  );
}




Widget backButtonLayoutWithTranparentBack(context) {
  return Align(
    alignment: Alignment.topLeft,
    child: IconButton(
      icon: SizedBox.fromSize(
        child: Icon(Icons.arrow_back,color: color_white,)/*Image.asset(
          'assets/images/close-black.svg',
        ),*/
        // size: Size(300.0, 400.0),
      ),
      onPressed: () => Navigator.of(context).pop(),
    ),
  );
}




