import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:http/http.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:photobooth/MyUtility/MyConstants.dart';
import 'package:photobooth/MyUtility/MyCustumUIs.dart';
import 'package:photobooth/MyUtility/MyUtils.dart';
import 'package:photobooth/MyUtility/theme/AllStrings.dart';
import 'package:photobooth/MyUtility/theme/myStyle.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:photobooth/apiService/web_service.dart';
import 'package:photobooth/model/UploadedImageListPojo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  SharedPreferences prefs;
  List<String> uploadedImagesList=List();
  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';
  var loaderValue=false;

  @override
  void initState() {
    loadShredPref();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: toolbarLayout("Lg Memories", context),
        body: Container(
          width: double.infinity,
            height: double.infinity,
            decoration: new BoxDecoration(
              /*image: DecorationImage(
                  image:  ExactAssetImage('assets/images/email_bg.png'),
                  fit: BoxFit.fill),*/
            ),
            child: ModalProgressHUD(
                inAsyncCall: loaderValue,
                child: SingleChildScrollView(child: _buildUIComposer()))));
  }

  _buildUIComposer(){
    return mainUI();
  }



  mainUI(){
     if(uploadedImagesList.length==0){
       return noImageUploadedUI();
     }else{
      return buildGridView();
     }
    }

    noImageUploadedUI(){
     return Container(
       width: double.infinity,
       margin: EdgeInsets.only(left: 60,right: 60,top: 110,bottom: 150),
       child: Column(
        /* mainAxisAlignment: MainAxisAlignment.center,
         crossAxisAlignment: CrossAxisAlignment.center,
        */ children: <Widget>[
           Container(
             margin: EdgeInsets.only(left: 30,right: 30),

             child: SizedBox(
               width: 130,
               height: 130,
               child: Image.asset('assets/images/face.png',
                 fit: BoxFit.cover,),
             ),
           ),

           SizedBox(height: 50,),

           Text("You have't added any\nmemories yet !",textAlign: TextAlign.center,style: TextStyle(color:color_grey,fontSize:20,fontWeight: FontWeight.normal)),



           SizedBox(height: 70,),

           GestureDetector(
             onTap: (){
               loadAssets();
             },
             child: Container(
               //  margin: EdgeInsets.only(bottom: 60),
               width: 150,
               height: 45,
               //margin: EdgeInsets.only(left: 60,right: 55),
               child: Image.asset('assets/images/add_btn.png',
                 fit: BoxFit.fill,
                 //height: double.infinity,
                 // width: double.infinity,
                 /* alignment: Alignment.center,*/),
             ),
           ),




         ],),
     );
    }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList.clear();
      resultList = await MultiImagePicker.pickImages(
        maxImages: 20,
        enableCamera: false,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#000000",
          actionBarTitle: "Select",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );


     // var filesList = List<String>();
      //filesList.add(croppedFile.path);
      uploadmultipleimage(resultList);


    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }


  Widget buildGridView() {
    return Container(
      margin: EdgeInsets.only(left: 25,right:25,top: 15,bottom: 40),
      child: Column(
        children: <Widget>[

          Container(
            height: 500,
            width: double.infinity,
            child: GridView.count(
              crossAxisCount: 3,
              children: List.generate(uploadedImagesList.length, (index) {
               // Asset asset = images[index];
                return Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: FadeInImage(
                      width: 50,
                      height: 50,
                      image: NetworkImage(MyConstants.IMAGE_BASE_URL+uploadedImagesList[index]), placeholder: AssetImage("assets/images/progress_image.png"))



               /*  Image.network(
                    MyConstants.IMAGE_BASE_URL+uploadedImagesList[index] ,
                    width: 50,
                    height: 50,
                  )*/,
                );
              }),
            ),
          ),


          SizedBox(height: 40,),

          GestureDetector(
            onTap: (){
              loadAssets();
            },
            child: Container(
              //  margin: EdgeInsets.only(bottom: 60),
              width: 150,
              height: 45,
              //margin: EdgeInsets.only(left: 60,right: 55),
              child: Image.asset('assets/images/addmore.png',
                fit: BoxFit.fill,
                //height: double.infinity,
                // width: double.infinity,
                /* alignment: Alignment.center,*/),
            ),
          ),
        ],

      ),
    );
  }


  getUploadedFilesApi() {

    setState(() {
      loaderValue = true;
    });
    var apicall;
    apicall = getUploadedFiles(prefs.get("userId"));
    apicall.then((response) {
      print(response.body);
      if (response.statusCode == 200) {
        setState(() {
          loaderValue = false;
        });
        final jsonResponse = json.decode(response.body);
        UploadedImageListPojo uploadedImageListPojo = UploadedImageListPojo.fromJson(jsonResponse);
        uploadedImagesList.clear();
        uploadedImagesList=uploadedImageListPojo.files;
        print(uploadedImagesList.toString());
        setState(() {

        });

      } else {
        setState(() {
          loaderValue = false;
        });
        MyUtils.showtoast(AllString.something_went_wrong);
      }
    }).catchError((error) {
      //print('error : $error');
      setState(() {
        loaderValue = false;
      });
      MyUtils.showtoast(AllString.something_went_wrong);
    });
  }

  void loadShredPref() async {
    prefs = await SharedPreferences.getInstance();
    getUploadedFilesApi();
  }


  Future uploadmultipleimage(images) async {
    setState(() {
      loaderValue = true;
    });
    String apiUrl = MyConstants.BASE_URL+'/photobooth/user/uploadFile';
    var uri = Uri.parse(apiUrl);
    http.MultipartRequest request = new http.MultipartRequest('POST', uri);
    //request.headers['accessToken'] = accessToken;
   // request.headers['Content-Type']="multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW";
    request.headers['Content-Type']="application/json";
   // request.headers['cache-control']="no-cache";

    List<MultipartFile> newList = new List<MultipartFile>();
    for (int i = 0; i < images.length; i++) {
    var path=  await FlutterAbsolutePath.getAbsolutePath(images[i].identifier);
    print(path);
      File imageFile = File(path);
     /* var stream =
      new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
     */ /*var length = await imageFile.length();
      var multipartFile = new http.MultipartFile("file", stream, length,
          filename: imageFile.path);*/
      http.MultipartFile multipartFile1 = await http.MultipartFile.fromPath('file', imageFile.path);
      newList.add(multipartFile1);
    }

    /*request.files.add(http.MultipartFile.fromBytes('file', await File.fromUri(Uri.parse(File(images[0].toString()).path)).readAsBytes(), contentType: new MediaType('application', 'x-tar')));
    var response = await request.send();
*/
    request.files.addAll(newList);
    var response = await request.send();

    response.stream.transform(utf8.decoder).listen((value) {
      // request.send().then((response){

      print(response.statusCode.toString());
      if (response.statusCode == 200) {
        setState(() {
          loaderValue = false;
        });
        print("Image Uploaded?");
        final jsonResponse = json.decode(value.toString());
         var files= jsonResponse['files'];
        saveFileApi(files);

      } else {
        setState(() {
          loaderValue = false;
        });
        MyUtils.showtoast(AllString.something_went_wrong);
        print("Upload Failed");
      }
    })
    ;
  }



  saveFileApi(files) {

    setState(() {
      loaderValue = true;
    });

      var req = {
      "files": files,
      "userId": prefs.get('userId').toString()
    };

    var jsonReqString = json.encode(req);
    print(jsonReqString);
    var apicall;
    apicall = saveFile(jsonReqString);
    apicall.then((response) {
      print(response.body);
      if (response.statusCode == 200) {
        setState(() {
          loaderValue = false;
        });
        final jsonResponse = json.decode(response.body);
        MyUtils.showtoast(jsonResponse['message']);
         getUploadedFilesApi();
      } else {
        setState(() {
          loaderValue = false;
        });
        MyUtils.showtoast(AllString.something_went_wrong);
      }
    }).catchError((error) {
      print('error : $error');
      setState(() {
        loaderValue = false;
      });
      MyUtils.showtoast(AllString.something_went_wrong);
    });
  }



}

