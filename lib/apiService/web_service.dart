import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'dart:async';
import 'dart:io';

import 'package:photobooth/MyUtility/MyConstants.dart';



Future<http.Response> deleteFile(fileName) async{
  final response = await http.get(MyConstants.BASE_URL+'/photobooth/user/deleteFile/'+fileName,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        //'XAPIKEY' : MyConstants.XAPIKEY,
        //'APPTOKEN' : apptoken
      },
      //body: request
  );
  return response;
}


Future<http.Response> getUserId() async{
  final response = await http.get(MyConstants.BASE_URL+'/photobooth/user/getUserId',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        //'XAPIKEY' : MyConstants.XAPIKEY,
        //'APPTOKEN' : apptoken
      },
     // body: request
  );
  return response;
}

Future<http.Response> getUploadedFiles(userId) async{
  final response = await http.get(MyConstants.BASE_URL+'/photobooth/user/getUploadedFiles/'+userId,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        //'XAPIKEY' : MyConstants.XAPIKEY,
        //'APPTOKEN' : apptoken
      },
      //body: request
  );
  return response;
}



Future<http.Response> saveFile(request) async{
  final response = await http.post(MyConstants.BASE_URL+'/photobooth/user/saveFile',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        //'XAPIKEY' : MyConstants.XAPIKEY,
        //'APPTOKEN' : apptoken
      },
      body: request
  );
  return response;
}




