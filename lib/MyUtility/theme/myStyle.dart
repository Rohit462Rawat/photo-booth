import 'package:flutter/material.dart';

TextStyle textStyle = const TextStyle(
    color: const Color(0XFFFFFFFF),
    fontSize: 16.0,
    fontWeight: FontWeight.normal);

ThemeData appTheme = new ThemeData(
  hintColor: Colors.white,
);

Color textFieldColor = const Color.fromRGBO(255, 255, 255, 0.1);


TextStyle buttonTextStyle = const TextStyle(
    color: const Color.fromRGBO(255, 255, 255, 0.8),
    fontSize: 14.0,
    fontFamily: "Roboto",
    fontWeight: FontWeight.bold);

 //  color hex codes

Color primaryColor = const Color(0xFF9D001B);
Color textfieldbackColor=const Color(0xFFDDDCDD);
Color color_white = const Color(0xFFFFFFFF);
Color color_light_grey = const Color(0xFFededed);
Color color_grey = const Color(0xFFA9A9A9);
Color color_darkgrey = const Color(0xFF989898);
Color color_dialog = const Color(0xFFF6F7F9);
Color color_imageBack= const Color(0xFFF6F6F6);


Color color_blackChat = const Color(0xFF313131);
Color color_greyChat = const Color(0xFFE9E9E9);



