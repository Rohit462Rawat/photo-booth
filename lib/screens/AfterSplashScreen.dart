import 'dart:async';
import 'package:flutter/material.dart';
import 'package:photobooth/screens/GetUserIdScreen.dart';

import 'package:shared_preferences/shared_preferences.dart';




class AfterSplashScreen extends StatefulWidget {
  @override
  _AfterSplashScreenState createState() => new _AfterSplashScreenState();
}

class _AfterSplashScreenState extends State<AfterSplashScreen> {
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: SafeArea(
        child: Container(
          child: Stack(
            children: <Widget>[
              Image.asset('assets/images/aftersplash_bg.png',
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity,
                /* alignment: Alignment.center,*/),

              Align(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => GetUserIdScreen()),
                            (Route<dynamic> route) => false);
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 60),
                    width: 220,
                    height: 70,
                    //margin: EdgeInsets.only(left: 60,right: 55),
                    child: Image.asset('assets/images/aftersplash_btn.png',
                      fit: BoxFit.fill,
                      //height: double.infinity,
                     // width: double.infinity,
                      /* alignment: Alignment.center,*/),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}