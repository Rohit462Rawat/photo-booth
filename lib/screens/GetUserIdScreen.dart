import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:photobooth/MyUtility/MyUtils.dart';
import 'package:photobooth/MyUtility/theme/AllStrings.dart';
import 'package:photobooth/apiService/web_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'HomeScreen.dart';

class GetUserIdScreen extends StatefulWidget {
  @override
  _GetUserIdScreenState createState() => _GetUserIdScreenState();
}

class _GetUserIdScreenState extends State<GetUserIdScreen> {
  var loaderValue=false;
  SharedPreferences prefs;
  var userId="";
  @override
  void initState() {
    loadShredPref();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            height: double.infinity,
            decoration: new BoxDecoration(
              image: DecorationImage(
                  image:  ExactAssetImage('assets/images/email_bg.png'),
                  fit: BoxFit.fill),
            ),
            child: ModalProgressHUD(
                inAsyncCall: loaderValue,
                child: SingleChildScrollView(child: _buildUIComposer()))));
  }


  void loadShredPref() async {
    prefs = await SharedPreferences.getInstance();
    getUserIdApi();
  }

    _buildUIComposer(){
    return Container(
      margin: EdgeInsets.only(left: 60,right: 60,top: 110,bottom: 150),
      child: Column(
        children: <Widget>[
      Container(
        margin: EdgeInsets.only(left: 30,right: 30),

        child: Image.asset('assets/images/splashlogo.png',
          fit: BoxFit.cover,),
      ),

          SizedBox(height: 90,),


          Text("Your User Id".toUpperCase(),style: TextStyle(fontSize:25,fontWeight: FontWeight.bold),),


          SizedBox(height: 20,),


          Text(userId.toUpperCase(),style: TextStyle(fontSize:22,fontWeight: FontWeight.bold),),


          SizedBox(height: 60,),

          GestureDetector(
            onTap: (){
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                  HomeScreen()), (Route<dynamic> route) => false);
            },
            child: Container(
            //  margin: EdgeInsets.only(bottom: 60),
              width: 220,
              height: 60,
              //margin: EdgeInsets.only(left: 60,right: 55),
              child: Image.asset('assets/images/btn.png',
                fit: BoxFit.fill,
                //height: double.infinity,
                // width: double.infinity,
                /* alignment: Alignment.center,*/),
            ),
          ),

          SizedBox(height: 60,),

          Text("You need to enter assigned user ID to\naccess the photos at the Kiosk.",textAlign: TextAlign.center,style: TextStyle(fontSize:15,fontWeight: FontWeight.normal),),




        ],),
    );
  }


  getUserIdApi() {

    setState(() {
      loaderValue = true;
    });
  /*  var req = {
      "email": email,
      "contactMethod": "Facebook",
      "deviceToken": fcmtoken
    };

    var jsonReqString = json.encode(req);
    print(jsonReqString);
*/
    var apicall;
    apicall = getUserId();
    apicall.then((response) {
      print(response.body);
      if (response.statusCode == 200) {
        setState(() {
          loaderValue = false;
        });
        final jsonResponse = json.decode(response.body);
       // if (jsonResponse['status'].toString() == "1") {
         userId=jsonResponse['userId'].toString();
          prefs.setBool('is_login', true);
          prefs.setString('userId', userId);
          setState(() {

          });

      /*  } else {
          MyUtils.showtoast(jsonResponse['message'].toString());
        }*/
      } else {
        setState(() {
          loaderValue = false;
        });
        MyUtils.showtoast(AllString.something_went_wrong);
      }
    }).catchError((error) {
      //print('error : $error');
      setState(() {
        loaderValue = false;
      });
      MyUtils.showtoast(AllString.something_went_wrong);
    });
  }


}
