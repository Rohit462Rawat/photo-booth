import 'package:flutter/material.dart';
import 'package:photobooth/screens/SplashScreen.dart';

import 'MyUtility/theme/AllStrings.dart';

void main() => runApp(MyApp());



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AllString.app_name,
      debugShowCheckedModeBanner: false,
      /*theme: ThemeData(accentColor: Colors.lightBlue,
          primarySwatch: Colors.pink
          ,primaryColor:  Colors.pink
      ),*/
      home: SplashScreen(),
    );
  }
}
