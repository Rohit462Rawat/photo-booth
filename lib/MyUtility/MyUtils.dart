import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:photobooth/MyUtility/theme/myStyle.dart';

class MyUtils{


  static showtoast(value) {

    Fluttertoast.showToast(
        msg: value,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: color_grey,
        textColor: Colors.black,
        fontSize: 16.0
    );
  }



}

class Item {
  const Item(this.name);
  final String name;
}
